//Soal 1
const luasPersegiPanjang = (panjang, lebar) => {
  let luas = panjang * lebar
  console.log(luas)
}
luasPersegiPanjang(5, 2)

const kelilingPersegiPanjang = (panjang, lebar) => {
  let keliling = 2 * (panjang + lebar)
  console.log(keliling)
}
kelilingPersegiPanjang(5, 2)

//Soal 2
//Dengan Arrow Function dan object Literal
const fullName = (fName, lName) => {
  const firstName = fName
  const lastName = lName
  const fullName = `${firstName} ${lastName}`
  console.log(fullName)
}
fullName("William", "Imoh")


//Soal 3
//Dengan destructuring
var biodata = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = biodata
// Driver code
console.log(firstName, lastName, address, hobby)

//Soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [west, east]
//Driver Code
console.log(combined)

//Soal 5
//Dengan template literals ES6
const planet = 'earth'
const view = 'glass'
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)
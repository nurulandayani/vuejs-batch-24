//Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var car1 = pertama.substr(0, 4);
var car2 = pertama.substr(12, 6);
var car3 = kedua.substr(0, 7);
var car4 = kedua.substr(8, 10);
var gabung = car1 + " " + car2 + " " + car3 + " " + car4.toUpperCase()
console.log(gabung); // saya senang belajar JAVASCRIPT

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var nilai1 = Number(kataPertama);
var nilai2 = Number(kataKedua);
var nilai3 = Number(kataKetiga);
var nilai4 = Number(kataKeempat);

var hitung1 = (nilai2 * nilai3) + nilai1 + nilai4;
console.log(hitung1);

//Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15);
var kataKetiga = kalimat.substring(15, 19);
var kataKeempat = kalimat.substring(19, 25);;
var kataKelima = kalimat.substring(25, 31);;

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

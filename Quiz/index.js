var split, trim = ''
function jumlah_kata(kalimat) {
    trim = kalimat.trim(" ")
    split = trim.split(" ")
    console.log(split.length)
}

//Contoh
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"
jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2

function next_date(tanggal, bulan, tahun) {
    var hari = 0
    var tgl, bln, thn
    switch (bulan) {
        case 1: { hari = 31; break; }
        case 2: {
            if (tahun % 4 != 0) { hari = 28 }
            else { hari = 29 }
            break;
        }
        case 3: { hari = 31; break; }
        case 4: { hari = 30; break; }
        case 5: { hari = 31; break; }
        case 6: { hari = 30; break; }
        case 7: { hari = 31; break; }
        case 8: { hari = 31; break; }
        case 9: { hari = 30; break; }
        case 10: { hari = 31; break; }
        case 11: { hari = 30; break; }
        case 12: { hari = 31; break; }
    }

    if (tanggal < hari && bulan <= 12) {
        tgl = tanggal + 1
        bln = bulan
        thn = tahun
    } else if (tanggal == hari && bulan < 12) {
        tgl = 1
        bln = bulan + 1
        thn = tahun
    } else if (tanggal == hari && bulan == 12) {
        tgl = 1
        bln = 1
        thn = tahun + 1
    } else { console.log("Tanggal inputan salah") }


    switch (bln) {
        case 1: { console.log(tgl + " Januari " + thn); break; }
        case 2: { console.log(tgl + " Februari " + thn); break; }
        case 3: { console.log(tgl + " Maret " + thn); break; }
        case 4: { console.log(tgl + " April " + thn); break; }
        case 5: { console.log(tgl + " Mei " + thn); break; }
        case 6: { console.log(tgl + " Juni " + thn); break; }
        case 7: { console.log(tgl + " Juli " + thn); break; }
        case 8: { console.log(tgl + " Agustus " + thn); break; }
        case 9: { console.log(tgl + " September " + thn); break; }
        case 10: { console.log(tgl + " Oktober " + thn); break; }
        case 11: { console.log(tgl + " November " + thn); break; }
        case 12: { console.log(tgl + " Desember " + thn); break; }
    }
}

//contoh 1
var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Maret 2020

//contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal, bulan, tahun) // output : 1 Maret 2021

//contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Januari 2021


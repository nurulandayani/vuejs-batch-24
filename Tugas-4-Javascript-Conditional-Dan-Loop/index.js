//Soal 1
console.log("===============================================================");
console.log("Jawaban soal 1 :");
var nilai;
nilai = 87;

if (nilai >= 85) {
    console.log("A");
} else if (nilai >= 75 || nilai < 85) {
    console.log("B");
} else if (nilai >= 65 || nilai < 75) {
    console.log("C");
} else if (nilai >= 55 || nilai < 65) {
    console.log("D");
} else {
    console.log("E");
}

//Soal 2
console.log("===============================================================");
console.log("Jawaban soal 2 :");
var tanggal = 05;
var bulan = 9;
var tahun = 1985;

switch (bulan) {
    case 1: { console.log(tanggal + " Januari " + tahun); break; }
    case 2: { console.log(tanggal + " Februari " + tahun); break; }
    case 3: { console.log(tanggal + " Maret " + tahun); break; }
    case 4: { console.log(tanggal + " April " + tahun); break; }
    case 5: { console.log(tanggal + " Mei " + tahun); break; }
    case 6: { console.log(tanggal + " Juni " + tahun); break; }
    case 7: { console.log(tanggal + " Juli " + tahun); break; }
    case 8: { console.log(tanggal + " Agustus " + tahun); break; }
    case 9: { console.log(tanggal + " September " + tahun); break; }
    case 10: { console.log(tanggal + " Oktober " + tahun); break; }
    case 11: { console.log(tanggal + " November " + tahun); break; }
    case 1: { console.log(tanggal + " Desember " + tahun); break; }
}

//Soal 3
console.log("===============================================================");
console.log("Jawaban soal 3 :");
var i, j;
var n = 7;
var pagar = '';
for (i = 0; i < n; i++) {
    for (j = i; j >= 0; j--) {
        pagar = pagar + "#";
    }
    console.log(pagar);
    pagar = '';
}

//Soal 4
console.log("===============================================================");
console.log("Jawaban soal 4 :");
var m = 10;
var i = 1;
var a = '';

while (i <= m) {
    if (i % 3 == 1) {
        console.log(i + " - I love programming");
        a = a + "=";
    } else if (i % 3 == 2) {
        console.log(i + " - I love Javascript");
        a = a + "=";
    } else if (i % 3 == 0) {
        console.log(i + " - I love VueJS");
        a = a + "=";
        console.log(a);
    }

    i++;
}



